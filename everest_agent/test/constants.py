# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

DUMMY_PATH = '../../../everest_agent/test/dummy.py'
DUMMY_LOOP_PATH = '../../../everest_agent/test/loop_dummy.py'
TM_TEST_PATH = '../../../everest_agent/test/tm_dummy.py'
DUMMY_WEB_PATH = '../../../everest_agent/test/web_dummy.py'
